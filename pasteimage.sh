#!/bin/bash
SIZE=800
# IMG - относительное имя для html файла
IMG=images/img`date +%s`.jpg
# IMGFULL - полный путь до картинки
IMGFULL=~/Dropbox/vimwiki/wiki.ilfumo.ru/html/$IMG
pngpaste $IMGFULL && echo "{{$IMG ||class=\"imgnewline\"}}"

# Изменяем размер картинки
# The "-Z" switch is really nice: it keeps the proportions of the original image, but makes sure neither the width or height exceed the dimensions specified
# >/dev/null 2>&1 - прячет вывод ошибок и текста. Нам они не нужно чтобы выставлялись
sips -Z "$SIZE"x"$SIZE" -s formatOptions 60 $IMGFULL >/dev/null 2>&1

# Сжимаем качество jpg картинки до 60%
# https://robservatory.com/use-sips-to-quickly-easily-and-freely-convert-image-files/
#sips -s formatOptions 60 $IMGFULL >/dev/null 2>&1
